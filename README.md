# Rect Redux

## Cách làm việc của Redux

### Cấu trúc

![Redux](https://images.viblo.asia/full/4613631f-1190-4f0a-9fe2-02b477946b05.png)

**Redux gồm 4 thành phần chính là**

- **Store :** Là 1 objetc lưu trữ state của toàn bộ ứng dụng, cho phép truy cập `state` qua `getState()`, update `state` qua `dispatch(action)`

- **Action :** Là nơi tạo ra các action dùng để mô tả event do người dùng tạo ra

- **Reducer :** Là 1 function nhận đầu vào là state và cá mô tả về event và dựa trên đó thay đổi state mà không làm thay đổi state cũ

- **View :** Hiển thị dữ liệu được cung cấp từ store

### Nguyên lí hoạt động của Redux

- **Bước 1:** Khi có 1 sự kiện ví dụ như người dùng click button,hay bất kì event nào với DOM thì thằng action sẽ sinh ra 1 action mô tả những gì đang xảy ra

- **Bước 2:** `Action` sẽ điều phối `Reducer` xử lý event thông qua hàm `dispatch(action)`

- **Bước 3:** `Reducer` dựa vào những mô tả của `Action` để cần biết thực hiện thay đổi gì trên `State` và thực hiện update
- **Bước 4**: Khi State được update thì các trigger đang theo dõi state đó sẽ nhận được thông tin update và tiến hành render lại phần view để hiển thị ra cho người dùng

### 3 nguyên tắc trong Redux

Redux được xây dựng dựa trên 3 nguyên lý :

- **Store** luôn là nguồn dữ liệu đúng và tin cậy nhất
- **State** chỉ được phép đọc, cách duy nhất để thay đổi `State` là phát sinh 1 action, và để reducer thay đổi state
- Các function Reducer phải là `Pure function` (với cùng 1 đầu vào và chỉ cho ra 1 đầu ra duy nhất)
